import org.junit.Assert;
import org.junit.Test;
import org.sikuli.script.App;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class TestSpotify {
    private static final String username = "spotmedown11";
    private static final String password = "spotmedown11";
    private final static Pattern usernameTxt = new Pattern(TestSpotify.class.getResource("login/username.png"));
    private final static Pattern passwordTxt = new Pattern(TestSpotify.class.getResource("login/password.png"));
    private final static Pattern loginButton = new Pattern(TestSpotify.class.getResource("login/loginbutton.png"));
    private final static Pattern loginSuccess = new Pattern(TestSpotify.class.getResource("login/usernameafterloging.png"));
    private final static Pattern loginFailure = new Pattern(TestSpotify.class.getResource("login/failedlogin.png"));
    private final static Pattern searchBar = new Pattern(TestSpotify.class.getResource("searchmusic/searchbar.png"));
    private final static Pattern settings = new Pattern(TestSpotify.class.getResource("logout/settings.png"));
    private final static Pattern logoutButton = new Pattern(TestSpotify.class.getResource("logout/logoutbutton.png"));
    private final static Pattern searchResult = new Pattern(TestSpotify.class.getResource("searchmusic/searchresult.png"));
    private final static Pattern addToPlayListLink = new Pattern(TestSpotify.class.getResource("playmusic/addtoplaylist.png"));
    private final static Pattern musicPlaying = new Pattern(TestSpotify.class.getResource("playmusic/musicinprogress.png"));
    private final static Pattern playButton = new Pattern(TestSpotify.class.getResource("playmusic/playbutton.png"));
    private final static Pattern newPlayListbutton = new Pattern(TestSpotify.class.getResource("createplaylist/newplaylistbutton.png"));
    private final static Pattern playlistNameInput = new Pattern(TestSpotify.class.getResource("createplaylist/playlistnameinput.png"));
    private final static Pattern playlistDescriptionInpput = new Pattern(TestSpotify.class.getResource("createplaylist/descriptioninput.png"));
    private final static Pattern createPlayListButton = new Pattern(TestSpotify.class.getResource("createplaylist/createbutton.png"));
    private final static Pattern playListArea = new Pattern(TestSpotify.class.getResource("createplaylist/addedplaylist.png"));

    @Test
    public void testLoginIsSuccessful() throws Exception {
        Screen s = new Screen();
        openApp();

        loginToApp(s, username, password);

        Assert.assertNotNull(s.exists(loginSuccess.similar(0.9f)));
        logoutFromApp(s);
    }

    private void loginToApp(Screen s, String username, String password) throws org.sikuli.script.FindFailed {
        s.wait(usernameTxt.similar(0.9f)).click();
        s.write(username);
        s.click(passwordTxt.similar(0.9f));
        s.write(password);
        s.click(loginButton.similar(0.9f));
    }

    @Test
    public void testLoginFails() throws Exception {
        Screen s = new Screen();
        openApp();

        loginToApp(s, "invalidusername", "invalidusername");

        Assert.assertNotNull(s.exists(loginFailure.similar(0.9f)));
        logoutFromApp(s);
    }

    @Test
    public void testSearchIsSuccessful() throws Exception {
        Screen s = new Screen();
        openApp();
        loginToApp(s, username, password);

        s.wait(searchBar.similar(0.9f)).click();
        s.write("rock");

        Assert.assertNotNull(s.exists(searchResult.similar(0.9f)));
        logoutFromApp(s);
    }

    @Test
    public void testPlayingMusicIsSuccessful() throws Exception {
        Screen s = new Screen();
        openApp();
        loginToApp(s, username, password);

        s.wait(searchBar.similar(0.9f)).click();
        s.write("rock");
        s.wait(searchResult.similar(0.9f)).click();
        s.wait(addToPlayListLink.similar(0.8f)).click();
        s.wait(playButton.similar(0.9f)).click();

        Assert.assertNotNull(s.exists(musicPlaying.similar(0.6f)));
        logoutFromApp(s);
    }

    /*
     * This test case tests the create playlist functionality
     * I chose to test this because it is very commonly used feature
     * and we need to ensure such core functionalities are well tested
     */
    @Test
    public void testCreatePlayListIsSuccessful() throws Exception {
        Screen s = new Screen();
        openApp();
        loginToApp(s, username, password);

        s.wait(newPlayListbutton.similar(0.9f)).click();
        s.click(playlistNameInput);
        s.write("myfavorite");
        s.click(playlistDescriptionInpput);
        s.write("Soulful music");
        s.click(createPlayListButton);

        Assert.assertNotNull(s.exists(playListArea.similar(0.8f)));
        logoutFromApp(s);
    }

    private void logoutFromApp(Screen s) throws org.sikuli.script.FindFailed {
        s.click(settings);
        s.wait(logoutButton).click();
    }

    private void openApp() {
        App app = new App("Spotify.app");
        app.open();
        app.focus();
    }
}
