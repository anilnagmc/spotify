##How to run the test cases
```
mvn test
```

##Assumptions
1. Java 8 and maven should be installed on the system
2. Spotify application should be installed on the system
3. Spotify backend services should be running
4. As it is an image based testing framework, major changes in the Spotify UI layout may fail the tests
5. The Spotify application is launched by the tests automatically. So, it should not be running before triggering the test cases